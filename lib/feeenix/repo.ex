defmodule Feeenix.Repo do
  use Ecto.Repo,
    otp_app: :feeenix,
    adapter: Ecto.Adapters.SQLite3
end
