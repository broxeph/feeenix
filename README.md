# Feeenix

In which our hero rewrites his personal homepage, or "PHP", in Phoenix!

Our [previous episode](https://github.com/broxeph/djangalex) was brought to you by Django, Postgres, Heroku, and the letters A, W, and S.

We, gentle reader, choose to use Erlang, an elegant weapon for a more civilized age—plus fly.io for hipster points.

## Run the Trap

1. Initialize local SQLite db: `mix ecto.create`
2. Do the thing: `mix phx.server`

## Stack

- Frontend
  - HEEx templates (Phoenix)
- Backend
  - TODO: Phoenix admin
  - Phoenix
  - Elixir
  - SQLite
- CDN
  - AWS CloudFront
- Infra
  - fly.io

## Overview

- One-page website with some personal links, a headshot, and a random subtitle
- Subtitle is pulled from SQLite
  - Despite being used everywhere, Postgres isn't always the best solution for simple, read-heavy sites

## To Do

- [x] Create Phoenix project
- [x] Copy old templates, CSS
- [ ] Convert Bootstrap classes to native CSS
- [ ] Add subtitle model
- [ ] Add auth
- [ ] Add admin backend
- [ ] Randomize subtitles
- [ ] Spruce up templates, CSS, make responsive
- [ ] Move assets from CloudFront to app server
- [ ] Add Google Analytics to app template (`app.html.heex`)
- [ ] Deploy to fly.io
- [ ] Set up CloudFlare email protection
- [ ] Enable LiveDashboard in prod, behind auth
